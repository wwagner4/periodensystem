## Das Periodensystem
- Wie hängt das Periodensystem mit dem Orbitalmodell zusammen ?
- Wie lassen sich die Lücken im Periodensystem erklären ?

### Das Orbitalmodell
Beschreibt jedes Element anhand der Werte seiner Quantenzahlen [QZ]. 
- n, HauptQZ, Schale: 1, 2, 3, 4, 5, ... oder K, L, M, N, ...
- l, NebenQZ, Bahndrehimpuls-QZ: 0, 1, 2, 3, ... oder s, p, d, f, g, h, i, ...
- m, MagnetQZ, Neigung des Drehimpulsvektors: z.B -2, -1, 0, 1, 2 für l = 2 
- s, SpinQZ: -1/2, + 1/2

Danke liebe Chemiker für die vielen Bezeichnungen.

#### Zusammenhang der Quantenzahlen

- n:  hat die Werte 1 - 7 (ist so, Elemente mit n > 7 wurden noch nicht entdeckt und wären falls sie existieren 
extrem instabil)
- l: = 0 ... (n - 1)
- m: = -l, (-l + 1), ... -1, 0, 1, ... (l - 1), l
- s: hat immer die Werte -1/2 oder + 1/2 

Beispiele:

|n   |l   |m    |s      |
|--- |--- |---  |---    |
|2   |1   |0    |+1/2   |
|1   |0   |0    |-1/2   |
|3   |2   |-2   |+1/2   |

Und viele mehr. Insgesamt gibt es (bei 7 HauptQZ) 286 gültige Kombinationen (Orbitale). Allerdings
gibt es nur 118 Elemente. 
- Was ist mit den restlichen Kombinationen? 
- Warum kommen sie in der Natur nicht vor?

TODO: Graphik

Arbeitshypothese: Es hängt mit den Energieniveaus der einzelnen Orbitale
zusammen. 

#### Berechnung des Enerieniveaus eines Orbitals

???
