package entelijan.periodensystem

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import java.nio.file.Files
import java.nio.file.Paths


fun main() {
    val lang = "en"
    val records = records(lang)

    val elems = readElementCells()
    print(elems.joinToString(separator = "\n") { it.toString() })

    // _headers(records)
    // _numberOfColumns(records)
    // _cols(records, 4)
}

/*
en
*  0 - AtomicNumber
*  1 - Symbol
*  2 - Element
   3 - OriginOfName
*  4 - Group
*  5 - Period
*  6 - AtomicWeight
   7 - Density
   8 - MeltingPoint
   9 - BoilingPoint
  10 - HeatCapacity
* 11 - ElectroNegativity
  12 - AbundanceInEarthCrust

de
   0 - Ordnungszahl
   1 - Symbol
*  2 - Elementname
*  3 - Elementkategorie
   4 - Kristallstruktur
   5 - Masse(u)
   6 - Dichte(kg/m³)(20 °C)
   7 - Schmelzpunkt(°C)
   8 - Siedepunkt(°C)
   9 - entdecktim Jahr
  10 - Entdecker
* 11 - Elektronenkonfiguration
  12 - Anzahl stabiler Isotope
*/

fun readElementCells(): Iterable<ElementCell> {

    fun createElementCell(en: CSVRecord, de: CSVRecord): ElementCell {
        return ElementCell(
            atomicNumber = en[0],
            symbol = en[1],
            name = en[2],
            nameDe = de[2],
            categoryDe = de[3],
            electroNegativity = en[11].toDoubleOrNull(),
            electroConfiguration = de[11],
            group = en[4].toIntOrNull(),
            period = en[5].toInt(),
            atomicWeight = en[6].toDoubleOrNull()
        )
    }

    val enRecords = records("en")
    val deRecords = records("de")
    val both = enRecords.zip(deRecords)
    return both.map { (enRec, deRec) -> createElementCell(enRec, deRec) }
}

private fun records(lang: String): List<CSVRecord> {
    val path = Paths.get("src", "main", "resources", "elemente-${lang}.csv")
    val br = Files.newBufferedReader(path, charset("utf-8"))
    val parser = CSVParser(
        br, CSVFormat.DEFAULT
            .withFirstRecordAsHeader()
    )
    return parser.records.toList()
}

private fun cols(records: Iterable<CSVRecord>, index: Int) {

    val values = records.asSequence().withIndex()
        .filter { it.index >= 0 }
        .map { "${it.value[1]} - ${it.value[index]}" }
        .withIndex()
        .sortedBy { it.index }
        .toList()
        .joinToString(separator = "\n") { "${it.index} \'${it.value}\'" }

    println(values)

}


private fun headers(lines: Iterable<CSVRecord>) {

    fun toValues(rec: CSVRecord): Iterable<String> {
        return (0 until rec.size())
            .map { rec.get(it) }
    }

    println(toValues(lines.first())
        .withIndex()
        .map { (i, v) -> "%5d - %s".format(i, v) }
        .joinToString(separator = "\n") { it })
}

private fun numberOfColumns(lines: Iterable<CSVRecord>) {

    val colnums = lines.withIndex()
        .map { (i, v) -> "%5d - %5d".format(i, v.size()) }

    print(colnums.joinToString(separator = "\n") { it })
}