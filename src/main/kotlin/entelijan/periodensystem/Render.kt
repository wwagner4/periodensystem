package entelijan.periodensystem

fun main() {
    renderReriodicSystem()
}

fun renderReriodicSystem() {
    val re = (1..7).map { i ->
        (1..98).map { j -> Pair(i, j) }
    }
    println(re)
}

abstract class Cell(x: Int, y: Int) : Renderable

data class ElementCell(
    val atomicNumber: String, val symbol: String, val name: String, val nameDe: String, val categoryDe: String,
    val group: Int?, val period: Int, val atomicWeight: Double?, val electroNegativity: Double?,
    val electroConfiguration: String
) : Renderable {

    override fun render(): String {
        return "<ElementCell to be done>"
    }
}

class IllegalCell : Renderable {

    override fun render(): String {
        return "<illegal to be done>"
    }
}


interface Renderable {

    fun render(): String

}

